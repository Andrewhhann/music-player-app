package com.example.myapplication;

import android.media.Image;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SongViewHolder extends RecyclerView.ViewHolder{

    // Constructor
    public SongViewHolder(@NonNull View itemView) {
        super(itemView);

        this.itemView = itemView;
        viewHolder_imageView = itemView.findViewById(R.id.viewHolder_imageView);
        songNameTextView = itemView.findViewById(R.id.song_name_textview);
        artistNameTextView = itemView.findViewById(R.id.artist_name_textview);
        selectedBackgroundView = itemView.findViewById(R.id.selected_background_view);
        songNameSelected = itemView.findViewById(R.id.song_name_selected);
        songGraphic = itemView.findViewById(R.id.song_graphic);
        songSelectedGraphic = itemView.findViewById(R.id.selected_song_graphic);

    }

    // Properties
    View itemView;
    ImageView viewHolder_imageView;
    TextView songNameTextView;
    TextView artistNameTextView;
    View selectedBackgroundView;
    TextView songNameSelected;
    ImageView songGraphic;
    ImageView songSelectedGraphic;
}
