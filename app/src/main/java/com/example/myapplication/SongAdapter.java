 package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongViewHolder> {

    // Constructor
    SongAdapter(@NonNull Context context, @NonNull ArrayList<Song> songs, @NonNull MainActivity mainActivity) {
        this.context = context;
        this.songs = songs;
        this.mainActivity = mainActivity;
    }
    // Overridden methods
    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // This method is called whenever I need to create a new ViewHolder
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_songs, parent, false);
        SongViewHolder viewHolder = new SongViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {
        // This method is called whenever an existing ViewHolder needs to be re-used
        // At this point, we need to repopulate the ViewHolder
        Song song = songs.get(position);
        holder.viewHolder_imageView.setImageResource(song.imageResource);
        holder.songNameTextView.setText(song.songName);
        holder.songNameSelected.setText(song.songName);
        holder.artistNameTextView.setText(song.artistName);

        // Set the selected background to be visible/invisible
        if (position == mainActivity.currentSongIndex) {
            holder.selectedBackgroundView.setVisibility(View.VISIBLE);
        }
        else {
            holder.selectedBackgroundView.setVisibility(View.INVISIBLE);
        }

        // Set the selected song name to be visible/invisible
        if (position == mainActivity.currentSongIndex) {
            holder.songNameSelected.setVisibility(View.VISIBLE);
            holder.songNameTextView.setVisibility(View.INVISIBLE);
        }
        else {
            holder.songNameSelected.setVisibility(View.INVISIBLE);
            holder.songNameTextView.setVisibility(View.VISIBLE);
        }

        // Set the selected song graphic to be visible/invisible
        if (position == mainActivity.currentSongIndex) {
            holder.songSelectedGraphic.setVisibility(View.VISIBLE);
            holder.songGraphic.setVisibility(View.INVISIBLE);
        }
        else {
            holder.songSelectedGraphic.setVisibility(View.INVISIBLE);
            holder.songGraphic.setVisibility(View.VISIBLE);
        }

        // Add button tap handler for this song item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // User tapped on this song item
                System.out.println("User tapped on song at index" + position);
                mainActivity.onUserSelectedSongAtPosition(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        // Called whenever the RecyclerView needs to know how many items in total to display
        return songs.size();
    }

    // Properties
    Context context;
    ArrayList<Song> songs;
    MainActivity mainActivity;
}
