package com.example.myapplication;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Runnable {

    // Tells Main Activity that user has selected song at position
    void onUserSelectedSongAtPosition(int position) {
        switchSong(currentSongIndex, position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateDataModel();

        connectXMLViews();

        setupRecyclerView();

        setupButtonHandlers();

        displayCurrentSong();

        displayNextSong();

        displayPreviousSong();
    }

    // Adding songs to the array of songs in the playlist
    void populateDataModel() {

        // Initialize properties of playlist
        playlist.name = "Royalty Free Playlist";
        playlist.songs = new ArrayList<Song>();

        // Create and initialize the first song
        Song song = new Song();
        song.songName = "Acoustic Breeze";
        song.artistName = "bensound.com";
        song.duration = 157;
        song.imageResource = R.drawable.acousticbreeze;
        song.mp3Resource = R.raw.acousticbreeze;

        // Adding the first song to the array of songs in the playlist
        playlist.songs.add(song);

        // Creating and initialize the second song
        song = new Song();
        song.songName = "A New Beginning";
        song.artistName = "bensound.com";
        song.duration = 154;
        song.imageResource = R.drawable.anewbeginning;
        song.mp3Resource = R.raw.anewbeginning;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Creative Minds";
        song.artistName = "bensound.com";
        song.duration = 147;
        song.imageResource = R.drawable.creativeminds;
        song.mp3Resource = R.raw.creativeminds;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Going Higher";
        song.artistName = "bensound.com";
        song.duration = 244;
        song.imageResource = R.drawable.goinghigher;
        song.mp3Resource = R.raw.goinghigher;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Happy Rock";
        song.artistName = "bensound.com";
        song.duration = 105;
        song.imageResource = R.drawable.happyrock;
        song.mp3Resource = R.raw.happyrock;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Hey";
        song.artistName = "bensound.com";
        song.duration = 171;
        song.imageResource = R.drawable.hey;
        song.mp3Resource = R.raw.hey;
        playlist.songs.add(song);

        song = new Song();
        song.songName = "Summer";
        song.artistName = "bensound.com";
        song.duration = 217;
        song.imageResource = R.drawable.summer;
        song.mp3Resource = R.raw.summer;
        playlist.songs.add(song);
    }

    // Connect each XML view with find ID
    void connectXMLViews() {
        songsRecyclerView = findViewById(R.id.song_list_view);
        previousButton = findViewById(R.id.previous_button);
        pauseButton = findViewById(R.id.pause_button);
        playButton = findViewById(R.id.play_button);
        nextButton = findViewById(R.id.next_button);
        loopButton = findViewById(R.id.loop_button);
        shuffleButton = findViewById(R.id.shuffle_button);
        leftCoverImageView = findViewById(R.id.cover_image_left);
        middleCoverImageView = findViewById(R.id.cover_image_middle);
        rightCoverImageView = findViewById(R.id.cover_image_right);
        soundWaveGraphicImageView = findViewById(R.id.soundWave_image);
        timeStampTextView = findViewById(R.id.timeStamp);
        songNameTextView = findViewById(R.id.songName);
        artistNameTextView = findViewById(R.id.artistName);
        songGraphic = findViewById(R.id.song_graphic);
        songSelectedGraphic = findViewById(R.id.selected_song_graphic);
        progressBar =  findViewById(R.id.progressBar);

    }

    void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        songsRecyclerView.setLayoutManager(layoutManager);

        // Connect the adapter to the recyclerView
        songAdapter = new SongAdapter(this, playlist.songs, this);
        songsRecyclerView.setAdapter(songAdapter);
    }

    void displayCurrentSong() {
        Song currentSong = playlist.songs.get(currentSongIndex);
        middleCoverImageView.setImageResource(currentSong.imageResource);
        songNameTextView.setText(currentSong.songName);
        artistNameTextView.setText(currentSong.artistName);
    }

    void displayPreviousSong() {
        if (currentSongIndex -1 < 0) {
            leftCoverImageView.setImageResource(playlist.songs.get(playlist.songs.size() -1).imageResource);
        }
        else {
            leftCoverImageView.setImageResource(playlist.songs.get(currentSongIndex -1).imageResource);
        }
    }

    void displayNextSong() {
        if (currentSongIndex +1 > playlist.songs.size()) {
            rightCoverImageView.setImageResource(playlist.songs.get(0).imageResource);
        }
        else {
            rightCoverImageView.setImageResource(playlist.songs.get(currentSongIndex +1).imageResource);
        }
    }

    void setupButtonHandlers() {

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Called when Previous button is tapped
                System.out.println("Previous button is tapped.");
                if (currentSongIndex -1 >= 0) {
                    switchSong(currentSongIndex, currentSongIndex - 1);
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Next button is tapped.");
                if (currentSongIndex + 1 < playlist.songs.size()) {
                    switchSong(currentSongIndex, currentSongIndex + 1);
                }
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Pause/play button is tapped.");
                playCurrentSong();
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Pause/play button is tapped.");
                pauseCurrentSong();
            }
        });

        loopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Loop button is tapped.");
                if (mediaPlayer.isLooping()) {
                    mediaPlayer.setLooping(false);
                    loopButton.setImageTintList(null);
                }
                else {
                    mediaPlayer.setLooping(true);
                    loopButton.setImageTintList(getColorStateList(R.color.green));
                }
            }
        });

        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Shuffle button is tapped.");
            }

        });

    }

    void switchSong(int fromIndex, int toIndex) {
        // Tell song adapter to refresh currently selected song
        songAdapter.notifyItemChanged(currentSongIndex);

        // Update current song index
        currentSongIndex = toIndex;

        // Display song
        displayCurrentSong();
        displayPreviousSong();
        displayNextSong();

        // Tell song adapter to refresh the newly selected song
        songAdapter.notifyItemChanged(currentSongIndex);

        // Scroll to make current song visible in recyclerview
        songsRecyclerView.scrollToPosition(currentSongIndex);

        // Check if a current song is playing
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.release();
        }
        mediaPlayer = null;
        playCurrentSong();

    }

    void pauseCurrentSong() {
        System.out.println("Pausing song at index" + currentSongIndex);

        // Check if media player already exists
        if (mediaPlayer != null) {
            // Media player exists, go ahead and pause it
            mediaPlayer.pause();
        }

        pauseButton.setVisibility(View.VISIBLE);
        playButton.setVisibility(View.INVISIBLE);
    }

    void playCurrentSong() {
        System.out.println("Playing song at index" + currentSongIndex);

        // Check if media player already exists
        if (mediaPlayer == null) {
            // mediaPlayer has not been created

            // Get the song object corresponding to the current song
            Song currentSong = playlist.songs.get(currentSongIndex);

            // Create a media player for the MP3 resource of the current song
            mediaPlayer = MediaPlayer.create(MainActivity.this, currentSong.mp3Resource);

            // Set progress bar max of current song
            progressBar.setMax(mediaPlayer.getDuration());
        }

        // Play the song
        mediaPlayer.start();
        pauseButton.setVisibility(View.INVISIBLE);
        playButton.setVisibility(View.VISIBLE);

        new Thread(this).start();
    }

    // Properties
    Playlist playlist = new Playlist();
    Integer currentSongIndex = 0;
    SongAdapter songAdapter;

    // MediaPlayer to play MP3
    MediaPlayer mediaPlayer = null;

    // XML views
    RecyclerView songsRecyclerView;
    ImageButton previousButton;
    ImageButton pauseButton;
    ImageButton playButton;
    ImageButton nextButton;
    ImageButton loopButton;
    ImageButton shuffleButton;
    ImageView leftCoverImageView;
    ImageView middleCoverImageView;
    ImageView rightCoverImageView;
    ImageView soundWaveGraphicImageView;
    TextView timeStampTextView;
    TextView songNameTextView;
    TextView artistNameTextView;
    ImageView songGraphic;
    ImageView songSelectedGraphic;
    ProgressBar progressBar;

    // Progress Bar
    @Override
    public void run() {
        int currentPosition = mediaPlayer.getCurrentPosition();
        int total = mediaPlayer.getDuration();

        while (mediaPlayer != null) {
            if (mediaPlayer.isPlaying() || currentPosition < total) {
                try {
                    //noinspection BusyWait
                    Thread.sleep(100);
                    currentPosition = mediaPlayer.getCurrentPosition();
                } catch (Exception e) {
                    return;
                }

                progressBar.setProgress(currentPosition);

                runOnUiThread(() -> {
                    timeStampTextView = findViewById(R.id.timeStamp);
                    int songPositionSeconds = (int)Math.ceil(mediaPlayer.getCurrentPosition() / 1000f);

                    String curentPositionTime;
                    if (songPositionSeconds % 60 < 10) {
                        curentPositionTime = (int)Math.ceil(songPositionSeconds / 60) + ":0" + songPositionSeconds % 60;
                    } else {
                        curentPositionTime = (int)Math.ceil(songPositionSeconds / 60) + ":" + songPositionSeconds % 60;
                    }
                    timeStampTextView.setText(curentPositionTime);
                });
            }
        }
    }
}